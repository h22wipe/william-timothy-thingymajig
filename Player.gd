extends KinematicBody2D

export var speed := 600.0

var velocity := Vector2.ZERO

func William_moveScript():
	var horizontal_direction = (
		Input.get_action_strength("move_right")-
		Input.get_action_strength("move_left")
	)
	var vertical_direction = (
		Input.get_action_strength("move_up")-
		Input.get_action_strength("move_down")
	)
	velocity = Vector2(horizontal_direction,vertical_direction).normalized() * speed
func _physics_process(delta):
	William_moveScript()
	velocity = move_and_slide(velocity)
